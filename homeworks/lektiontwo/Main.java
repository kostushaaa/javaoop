package lektiontwo;

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("Barsik","mouses", "red", 4);
        Dog dog = new Dog("Richi","meat", "gold", 13);

        System.out.println(cat.getVoice() + "\n" + dog.getVoice());
        dog.eat();
        cat.eat();

        dog.sleep();
        cat.sleep();

        System.out.println(cat + "\n" + dog);

        Veterinarian veterinarian = new Veterinarian("Johnson");
        veterinarian.treatment(cat);
    }
}
