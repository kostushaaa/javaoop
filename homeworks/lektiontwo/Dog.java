package lektiontwo;

public class Dog extends Animal {
    private String name;

    public Dog(String name,String ration, String color, int weight) {
        super(ration, color, weight);
        this.name = name;
    }

    public Dog() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getVoice() {
        return "gaw, gaw";
    }

    @Override
    public void eat() {
        System.out.println("I do eat such good food, thank you owner, gaw");
    }

    @Override
    public void sleep() {
        System.out.println("I do sleep, gaw");
    }

    @Override
    public String toString() {
        return String.format("%s{name='%s', ration='%s', color='%s', weight=%d}",
                getClass().getSimpleName(),
                getName(),
                getRation(),
                getColor(),
                getWeight());
    }
}
