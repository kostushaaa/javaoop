package lektiontwo;

public class Cat extends Animal {
    private String name;

    public Cat(String name,String ration, String color, int weight) {
        super(ration, color, weight);
        this.name = name;
    }

    public Cat() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getVoice() {
        return "meow meow";
    }

    @Override
    public void eat() {
        System.out.println("i do eat, meow");
    }

    @Override
    public void sleep() {
        System.out.println("i do sleep, meow");
    }


    @Override
    public String toString() {
        return String.format("%s{name='%s', ration='%s', color='%s', weight=%d}",
                getClass().getSimpleName(),
                getName(),
                getRation(),
                getColor(),
                getWeight());
    }
}
