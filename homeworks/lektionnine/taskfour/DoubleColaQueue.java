package lektionnine.taskfour;

import java.util.ArrayList;

public class DoubleColaQueue {
    public static void main(String[] args) {
        ArrayList<String> queue = new ArrayList<>();
        queue.add("Sheldon");
        queue.add("Leonard");
        queue.add("Volovitc");
        queue.add("Kutrapalli");
        queue.add("Penny");

        int totalCups = 2;

        System.out.println(queue);

        for (int i = 1; i <= totalCups; i++) {
            String str = queue.get(0);

            queue.remove(0);

            queue.add(str);
            queue.add(str);
        }

        System.out.println(queue);
    }
}
