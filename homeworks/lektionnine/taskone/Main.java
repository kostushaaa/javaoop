package lektionnine.taskone;

import java.util.ArrayList;
import java.util.List;

//Написать метод, который создаст список, положит в него 10
//элементов, затем удалит первые два и последний, а затем выведет
//результат на экран.
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list);

        list.remove(0);
        list.remove(0);
        list.remove(list.size() - 1);

        System.out.println(list);
    }
}
