package lektionnine.tasktwo.exceptions;

public class GroupOverflowException extends Exception {
    public GroupOverflowException(String message) {
        super(message);
    }
}
