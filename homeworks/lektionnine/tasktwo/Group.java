package lektionnine.tasktwo;

import lektionnine.tasktwo.exceptions.GroupOverflowException;
import lektionnine.tasktwo.exceptions.StudentNotFoundException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Group {
    private String groupName;
    private List<Student> students = new ArrayList<>();

    public Group(String groupName, Student[] students) throws GroupOverflowException {
        this.groupName = groupName;
        if (students != null) {
            setStudents(students);
        }
    }

    public Group() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(Student[] students) throws GroupOverflowException {
        if (students.length > this.students.size()) {
            throw new GroupOverflowException("Group capacity exceeded. Cannot add more students.");
        }

        this.students = new ArrayList<>(List.of(students));
    }

    public void addStudent(Student student) throws GroupOverflowException {
        if (students.size() >= Integer.MAX_VALUE) {
            throw new GroupOverflowException("Group is full. Cannot add more students.");
        }
        students.add(student);
    }

    public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
        for (Student student : students) {
            if (student != null && lastName.equals(student.getLastName())) {
                return student;
            }
        }
        throw new StudentNotFoundException(String.format("Student with lastname: %s does not exist", lastName));
    }

    public Student searchStudentByID(int id) throws StudentNotFoundException {
        for (Student student : students) {
            if (student != null && id == student.getId()) {
                return student;
            }
        }
        throw new StudentNotFoundException(String.format("Student with id: %d does not exist", id));
    }

    public boolean hasEquivalentStudent(Student studentToCheck) {
        return students.stream().anyMatch(student -> student != null && student.equals(studentToCheck));
    }

    public boolean removeStudentById(int id) {
        for (int i = 0; i < students.size(); i++) {
            Student student = students.get(i);
            if (student != null && id == student.getId()) {
                students.remove(i);
                return true;
            }
        }
        return false;
    }

    public void sortStudentByLastName() {
        students.sort(Comparator.nullsFirst(Comparator.comparing(Student::getLastName)));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Group: ").append(groupName).append("\n");
        sb.append("Students:\n");
        for (Student student : students) {
            if (student != null) {
                sb.append("Name: ").append(student.getName()).append(" ").append(student.getLastName())
                        .append(", Gender: ").append(student.getGender())
                        .append(", ID: ").append(student.getId()).append("\n");
            }
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(groupName, group.groupName) &&
                Objects.equals(students, group.students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupName, students);
    }
}
