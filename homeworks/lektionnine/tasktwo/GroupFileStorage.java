package lektionnine.tasktwo;

import lektionnine.tasktwo.exceptions.GroupOverflowException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GroupFileStorage {
    public void saveGroupToCSV(Group group) {
        String fileName = group.getGroupName() + ".csv";
        try (PrintWriter writer = new PrintWriter(new FileWriter(fileName))) {
            for (Student student : group.getStudents()) {
                if (student != null) {
                    writer.println(studentToCSV(student));
                }
            }
        } catch (IOException e) {
            System.out.println("Failed to save group to CSV: " + e.getMessage());
        }
    }

    public Group loadGroupFromCSV(File file) {
        String groupName = extractGroupName(file.getName());
        List<Student> students = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Student student = parseStudentFromCSV(line);
                if (student != null) {
                    students.add(student);
                }
            }
        } catch (IOException e) {
            System.out.println("Failed to load group from CSV: " + e.getMessage());
        }
        Student[] studentsArray = students.toArray(new Student[0]);
        try {
            return new Group(groupName, studentsArray);
        } catch (GroupOverflowException e) {
            System.out.println("Failed to create group: " + e.getMessage());
        }
        return null;
    }

    public File findFileByGroupName(String groupName, File workFolder) {
        File[] files = workFolder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile() && file.getName().startsWith(groupName) && file.getName().endsWith(".csv")) {
                    return file;
                }
            }
        }
        return null;
    }

    private String studentToCSV(Student student) {
        return student.getName() + "," + student.getLastName() + "," + student.getGender().toString() + "," + student.getId() + "," + student.getGroupName();
    }

    private Student parseStudentFromCSV(String line) {
        String[] values = line.split(",");
        if (values.length == 5) {
            String name = values[0];
            String lastName = values[1];
            Gender gender = Gender.valueOf(values[2]);
            int id = Integer.parseInt(values[3]);
            String groupName = values[4];
            return new Student(name, lastName, gender, id, groupName);
        }
        return null;
    }

    private String extractGroupName(String fileName) {
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex != -1) {
            return fileName.substring(0, dotIndex);
        }
        return fileName;
    }
}
