package lektionnine.taskthree;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        File file = new File("/home/kostia/tutorial/javaoop/homeworks/lektionnine/text.txt");
        String content = readFromFile(file);
        Map<Character, Integer> frequencies = countFrequencies(content);
        printFrequencies(frequencies);
    }

    public static String readFromFile(File filename) {
        String content = "";
        try {
            FileInputStream fis = new FileInputStream(filename);
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fis.read(buffer)) != -1) {
                content += new String(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            System.out.println("Error reading file: " + e.getMessage());
        }
        return content;
    }

    public static Map<Character, Integer> countFrequencies(String content) {
        Map<Character, Integer> frequencies = new HashMap<>();
        for (char c : content.toCharArray()) {
            frequencies.put(c, frequencies.getOrDefault(c, 0) + 1);
        }
        return frequencies;
    }

    public static void printFrequencies(Map<Character, Integer> frequencies) {
        for (Map.Entry<Character, Integer> entry : frequencies.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}
