package lektionfour;

import lektionfour.conrollers.Controller;
import lektionfour.exceptions.GroupOverflowException;
import lektionfour.exceptions.StudentNotFoundException;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();

        try {
            Student student1 = controller.createStudent();
            Student student2 = controller.createStudent();
            Student student3 = controller.createStudent();
            Student student4 = controller.createStudent();
            Student student5 = controller.createStudent();
            Student student6 = controller.createStudent();
            Student student7 = controller.createStudent();
            Student student8 = controller.createStudent();
            Student student9 = controller.createStudent();
            Student student10 = controller.createStudent();

            Group group = new Group("Group 1", null);

            group.addStudent(student1);
            group.addStudent(student2);
            group.addStudent(student3);
            group.addStudent(student4);
            group.addStudent(student5);
            group.addStudent(student6);
            group.addStudent(student7);
            group.addStudent(student8);
            group.addStudent(student9);
            group.addStudent(student10);

            System.out.println(group);

            try {
                Student foundStudent = group.searchStudentByLastName("Smith");
                System.out.println("Found Student: " + foundStudent);

                foundStudent = group.searchStudentByLastName("Johnson12");
                System.out.println("Found Student: " + foundStudent);
            } catch (StudentNotFoundException e) {
                System.out.println("Student Not Found: " + e.getMessage());
            }

            boolean removed = group.removeStudentById(2);
            System.out.println("Student Removed: " + removed);

            removed = group.removeStudentById(13);
            System.out.println("Student Removed: " + removed);

            System.out.println(group);

            System.out.println("Started");

            group.sortStudentByLastName();

            System.out.println(group);

        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException: " + e.getMessage());
        } catch (GroupOverflowException e) {
            System.out.println("Group Overflow: " + e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("NullPointerException: " + e.getMessage());
        }
    }
}
