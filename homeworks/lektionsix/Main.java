package lektionsix;

import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {
        int number = 100;

        long start = System.currentTimeMillis();
        BigInteger factorial = calculateFactorial(number);
        long end = System.currentTimeMillis();

        System.out.println("Простой алгоритм: Факториал числа " + number + " равен " + factorial);
        System.out.println("Время выполнения простого алгоритма: " + (end - start) + " миллисекунд");

        start = System.currentTimeMillis();
        Thread[] threads = new Thread[100];

        for (int i = 1; i <= threads.length; i++) {
            FactorialThread factorialThread = new FactorialThread(i);
            threads[i - 1] = new Thread(factorialThread);
            threads[i - 1].start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        end = System.currentTimeMillis();
        System.out.println("Многопоточный алгоритм: Время выполнения: " + (end - start) + " миллисекунд");
    }

    private static BigInteger calculateFactorial(int number) {
        BigInteger factorial = BigInteger.ONE;
        for (int i = 2; i <= number; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }
}
