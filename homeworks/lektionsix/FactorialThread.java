package lektionsix;

import java.math.BigInteger;
import java.util.function.BiFunction;

public class FactorialThread implements Runnable {
    private final int number;

    public FactorialThread(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        BigInteger factorial = calculateFactorial(number);
        System.out.println(Thread.currentThread().getId() + " -> Факториал числа " + number + " равен " + factorial);
    }

    private BigInteger calculateFactorial(int number) {
        BigInteger factorial = BigInteger.ONE;
        for (int i = 2; i <= number ; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }
}
