package lektionseven.tasktwo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPoolCopy {

    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 10; i++) {
            File file = new File("file" + i + ".txt");
            file.createNewFile();
        }

        File[] sourceFiles = new File[10];
        for (int i = 0; i < 10; i++) {
            sourceFiles[i] = new File("file" + i + ".txt");
        }
        File destination = new File("destination.txt");

        FileChannel[] sourceChannels = new FileChannel[10];
        for (int i = 0; i < 10; i++) {
            sourceChannels[i] = new FileInputStream(sourceFiles[i]).getChannel();
        }

        FileChannel destinationChannel = new FileOutputStream(destination).getChannel();

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 1024);

        long bytesCopied = 0;
        for (int i = 0; i < 10; i++) {
            while (sourceChannels[i].read(buffer) != -1) {
                buffer.flip();
                destinationChannel.write(buffer);
                bytesCopied += buffer.limit();
                System.out.print("\rCopied " + bytesCopied + " bytes");
                buffer.clear();
            }
        }

        for (FileChannel sourceChannel : sourceChannels) {
            sourceChannel.close();
        }
        destinationChannel.close();

        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("\nCopy is finished.");
    }
}