package lektionseven.taskthree;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MultiThreadedFileSearch {

    private static final int NUM_THREADS = 4; // Количество потоков для поиска

    public static void main(String[] args) {
        String filename = "source.txt";
        String searchDirectory = "/home/kostia/tutorial/javaoop";

        List<String> foundFiles = searchFiles(filename, new File(searchDirectory));

        if (foundFiles.isEmpty()) {
            System.out.println("Файлы с таким названием не найдены.");
        } else {
            System.out.println("Найденные файлы с названием '" + filename + "':");
            for (String filePath : foundFiles) {
                System.out.println(filePath);
            }
        }
    }

    private static List<String> searchFiles(String filename, File directory) {
        List<String> foundFiles = new ArrayList<>();

        ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);

        searchFilesInDirectory(filename, directory, executor, foundFiles);

        executor.shutdown();

        try {
            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return foundFiles;
    }

    private static void searchFilesInDirectory(String filename, File directory, ExecutorService executor, List<String> foundFiles) {
        File[] files = directory.listFiles();
        if (files == null) {
            return;
        }

        for (File file : files) {
            if (file.isDirectory()) {
                executor.execute(() -> searchFilesInDirectory(filename, file, executor, foundFiles));
            } else if (file.getName().equals(filename)) {
                foundFiles.add(file.getAbsolutePath());
            }
        }
    }
}
