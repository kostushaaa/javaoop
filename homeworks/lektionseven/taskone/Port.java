package lektionseven.taskone;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Port {

    public static void main(String[] args) {
        Ship ship1 = new Ship("Ship 1", 10);
        Ship ship2 = new Ship("Ship 2", 10);
        Ship ship3 = new Ship("Ship 3", 10);

        Dock dock1 = new Dock("Dock 1");
        Dock dock2 = new Dock("Dock 2");

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        executorService.execute(() -> {
            System.out.println("Ship 1 is unloading at Dock 1");
            int progress = 0;
            for (int i = 1; i <= ship1.getNumberOfBoxes(); i++) {
                try {
                    TimeUnit.SECONDS.sleep((int) 0.5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Ship 1 unloaded box #" + i);
                progress++;
                System.out.println("Progress: " + progress * 10 + "%");
            }
            System.out.println("Ship 1 has finished unloading");
        });

        executorService.execute(() -> {
            System.out.println("Ship 2 is unloading at Dock 1");
            int progress = 0;
            for (int i = 1; i <= ship2.getNumberOfBoxes(); i++) {
                try {
                    TimeUnit.SECONDS.sleep((int) 0.5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Ship 2 unloaded box #" + i);
                progress++;
                System.out.println("Progress: " + progress * 10 + "%");
            }
            System.out.println("Ship 2 has finished unloading");
        });

        executorService.execute(() -> {
            System.out.println("Ship 3 is unloading at Dock 2");
            int progress = 0;
            for (int i = 1; i <= ship3.getNumberOfBoxes(); i++) {
                try {
                    TimeUnit.SECONDS.sleep((int) 0.5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Ship 3 unloaded box #" + i);
                progress++;
                System.out.println("Progress: " + progress * 10 + "%");
            }
            System.out.println("Ship 3 has finished unloading");
        });

        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
