package lektionseven.taskone;

class Dock {

    private String name;

    public Dock(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
