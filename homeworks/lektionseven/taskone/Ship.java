package lektionseven.taskone;

class Ship {

    private String name;
    private int numberOfBoxes;

    public Ship(String name, int numberOfBoxes) {
        this.name = name;
        this.numberOfBoxes = numberOfBoxes;
    }

    public int getNumberOfBoxes() {
        return numberOfBoxes;
    }

}
