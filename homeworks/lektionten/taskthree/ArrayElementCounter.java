package lektionten.taskthree;

import java.util.HashMap;

public class ArrayElementCounter {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 1, 2, 4, 4, 5, 2, 3};

        HashMap<Integer, Integer> elementCount = new HashMap<>();

        for (int num : array) {
            elementCount.put(num, elementCount.getOrDefault(num, 0) + 1);
        }

        for (int num : elementCount.keySet()) {
            System.out.println("Number " + num + " occurs " + elementCount.get(num) + " times.");
        }
    }
}
