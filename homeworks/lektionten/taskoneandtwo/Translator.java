package lektionten.taskoneandtwo;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class Translator {

    private HashMap<String, String> dictionary;

    public Translator() {
        dictionary = new HashMap<>();
    }

    public void addToDictionary(String englishWord, String ukrainianWord) {
        dictionary.put(englishWord, ukrainianWord);
    }

    public void saveDictionaryToFile(String filePath) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (String englishWord : dictionary.keySet()) {
                String ukrainianWord = dictionary.get(englishWord);
                writer.write(englishWord + "=" + ukrainianWord);
                writer.newLine();
            }
        }
    }

    public void translateFile(String inputFilePath, String outputFilePath) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
             BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {

            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split("\\s+");
                for (String word : words) {
                    if (dictionary.containsKey(word)) {
                        writer.write(dictionary.get(word) + " ");
                    } else {
                        writer.write(word + " ");
                    }
                }
                writer.newLine();
            }
        }
    }

    public static void main(String[] args) {
        Translator translator = new Translator();
        translator.addToDictionary("hello", "привіт");
        translator.addToDictionary("world", "світ");
        translator.addToDictionary("example", "приклад");

        try {
            translator.translateFile("English.in", "Ukrainian.out");
            translator.saveDictionaryToFile("dictionary.txt");
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
