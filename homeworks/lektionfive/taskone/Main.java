package lektionfive.taskone;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            FileService.copyFilesWithExtension(new File("/home/kostia/tutorial/javaoop/homeworks/lektionfive/taskone/directory"), new File("/home/kostia/tutorial/javaoop/homeworks/lektionfive/taskone/target"), "jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
