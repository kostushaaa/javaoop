package lektionfive.taskone;

import java.io.*;

public class FileService {

    public static long copyFile(File fileIn, File fileOut) throws IOException {

        try (InputStream inputStream = new FileInputStream(fileIn); OutputStream outputStream = new FileOutputStream(fileOut)) {
            return inputStream.transferTo(outputStream);
        }
    }

    public static void copyFilesWithExtension(File sourceDirectory, File targetDirectory, String fileExtension) throws IOException {
        File sourceDir = new File(sourceDirectory.toString());
        File[] files = sourceDir.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.isFile() && file.getName().endsWith(fileExtension)) {
                    File fileOut = new File(targetDirectory, file.getName());
                    copyFile(file, fileOut);
                    System.out.println("Успешно");
                }
            }
        }
    }

}
