package lektionfive.tasktwo;

import lektionfive.tasktwo.exceptions.GroupOverflowException;
import lektionfive.tasktwo.exceptions.StudentNotFoundException;

import java.util.Arrays;
import java.util.Comparator;

public class Group {
    private String groupName;
    private Student[] students = new Student[10];
    private int studentCount = 0;

    public Group(String groupName, Student[] students) throws GroupOverflowException {
        this.groupName = groupName;
        if (students != null) {
            setStudents(students);
        }
    }

    public Group() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) throws GroupOverflowException {
        if (students.length > this.students.length) {
            throw new GroupOverflowException("Group capacity exceeded. Cannot add more students.");
        }

        this.students = Arrays.copyOf(students, students.length);
        this.studentCount = students.length;
    }

    public void addStudent(Student student) throws GroupOverflowException {
        if (studentCount >= students.length) {
            throw new GroupOverflowException("Group is full. Cannot add more students.");
        }
        students[studentCount] = student;
        studentCount++;
    }

    public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
        for (int i = 0; i < studentCount; i++) {
            Student student = students[i];
            if (student != null && lastName.equals(student.getLastName())) {
                return student;
            }
        }
        throw new StudentNotFoundException(String.format("Student with lastname: %s does not exist", lastName));
    }

    public Student searchStudentByID(int id) throws StudentNotFoundException {
        for (int i = 0; i < studentCount; i++) {
            Student student = students[i];
            if (student != null && id == student.getId()) {
                return student;
            }
        }
        throw new StudentNotFoundException(String.format("Student with id: %d does not exist", id));
    }

    public boolean removeStudentById(int id) {
        for (int i = 0; i < studentCount; i++) {
            Student student = students[i];
            if (student != null && id == student.getId()) {
                students[i] = null;
                studentCount--;
                for (int j = i; j < studentCount; j++) {
                    students[j] = students[j + 1];
                }
                students[studentCount] = null;
                return true;
            }
        }
        return false;
    }

    public void sortStudentByLastName() {
        Arrays.sort(students, Comparator.nullsFirst(Comparator.comparing(Student::getLastName)));
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Group: ").append(groupName).append("\n");
        sb.append("Students:\n");
        for (int i = 0; i < studentCount; i++) {
            Student student = students[i];
            if (student != null) {
                sb.append("Name: ").append(student.getName()).append(" ").append(student.getLastName())
                        .append(", Gender: ").append(student.getGender())
                        .append(", ID: ").append(student.getId()).append("\n");
            }
        }
        return sb.toString();
    }
}
