package lektionfive.tasktwo;

import lektionfive.tasktwo.exceptions.GroupOverflowException;
import lektionfive.tasktwo.exceptions.StudentNotFoundException;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        GroupFileStorage fileStorage = new GroupFileStorage();
        Group group = null;

        try {
            Student[] students = new Student[10];
            students[0] = new Student("John", "Doe", Gender.MALE, 1, "group 1");
            students[1] = new Student("Jane", "Smith", Gender.FEMALE, 2, "group 1");
            students[2] = new Student("Alex", "Johnson", Gender.MALE, 3, "group 1");
            students[3] = new Student("Emily", "Davis", Gender.FEMALE, 4, "group 1");
            students[4] = new Student("Michael", "Wilson", Gender.MALE, 5, "group 1");
            students[5] = new Student("Emma", "Brown", Gender.FEMALE, 6, "group 1");
            students[6] = new Student("William", "Jones", Gender.MALE, 7, "group 1");
            students[7] = new Student("Olivia", "Miller", Gender.FEMALE, 8, "group 1");
            students[8] = new Student("James", "Anderson", Gender.MALE, 9, "group 1");
            students[9] = new Student("Sophia", "Taylor", Gender.FEMALE, 10, "group 1");

            group = new Group("Group 1", students);

            System.out.println(group);

            fileStorage.saveGroupToCSV(group);

            File groupFile = fileStorage.findFileByGroupName(group.getGroupName(), new File("."));
            if (groupFile != null) {
                Group loadedGroup = fileStorage.loadGroupFromCSV(groupFile);
                if (loadedGroup != null) {
                    System.out.println("Loaded Group:\n" + loadedGroup);
                } else {
                    System.out.println("Failed to load group from CSV.");
                }
            } else {
                System.out.println("Group file not found.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException: " + e.getMessage());
        } catch (GroupOverflowException e) {
            System.out.println("Group Overflow: " + e.getMessage());
        }
    }
}
