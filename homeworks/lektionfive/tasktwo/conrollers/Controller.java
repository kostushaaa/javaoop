package lektionfive.tasktwo.conrollers;

import lektionfive.tasktwo.Gender;
import lektionfive.tasktwo.Student;

import java.util.Scanner;

public class Controller {
    public Student createStudent() throws IllegalArgumentException {
        Scanner scanner = new Scanner(System.in);
        String name;
        String lastName;
        Gender gender;
        int id;
        String groupName;

        System.out.println("Please enter next data about student you want to add: ");
        System.out.println("Name: ");
        name = scanner.nextLine();
        System.out.println("LastName: ");
        lastName = scanner.nextLine();
        while (true){
         try{
             System.out.println("Gender: MALE|FEMALE");
             String checkBuff = scanner.nextLine();
             checkBuff = checkBuff.toUpperCase();
             if (!checkBuff.equals(Gender.MALE.toString()) && !checkBuff.equals(Gender.FEMALE.toString())) {
                 throw new IllegalArgumentException("Gender cannot be " + checkBuff);
             }
             gender = Gender.valueOf(checkBuff);
             break;
         }
         catch (IllegalArgumentException e){
             System.out.println("Invalid GENDER. Please enter a valid Gender.");
         }
        }

        while (true) {
            try {
                System.out.println("ID: ");
                id = Integer.parseInt(scanner.nextLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid ID. Please enter a valid integer.");
            }
        }

        System.out.println("GroupName: ");
        groupName = scanner.nextLine();

        return new Student(name, lastName, gender, id, groupName);
    }
}
