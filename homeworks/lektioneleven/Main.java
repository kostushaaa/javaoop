package lektioneleven;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        String linkURL = "https://www.google.com";

        String htmlContent = "";

        URLConnection connection;

        try {
            connection = new URL(linkURL).openConnection();
            Scanner scanner = new Scanner(connection.getInputStream());
            scanner.useDelimiter("\\Z");
            htmlContent = scanner.next();
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        Document document = Jsoup.parse(htmlContent);
        Elements tags = document.select("a");

        List<String> linksHtml = HtmlParser.getTagsAttributeByName(tags, "href");

        System.out.println(linksHtml);

        Pattern patternHTTP = Pattern.compile("://");

        List<String> links = new ArrayList<>();

        for (String element : linksHtml) {
            Matcher matcher = patternHTTP.matcher(element);
            if (!matcher.find()) {
                if (element.charAt(0) == '/') {
                    element = linkURL + element;
                } else {
                    element = linkURL + "/" + element;
                }
            }
            links.add(element);
        }

        System.out.println(links);

        //Task two
        for(String url : links) {
            try {
                HtmlParser.checkAvailability(new URL(url));
            } catch (MalformedURLException e) {
                e.printStackTrace();
                continue;
            }
            System.out.println(url + " - Success");
        }

    }
}
