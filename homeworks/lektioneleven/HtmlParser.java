package lektioneleven;


import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HtmlParser {

    public static List<String> getTagsAttributeByName(Elements tags, String attribute) {
        List<String> result = new ArrayList<>();
        for (Element element : tags) {
            String href = element.attr(attribute);
            result.add(href);
        }
        return result;
    }

    public static Boolean checkAvailability(URL link) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) link.openConnection();
            int responseCode = httpURLConnection.getResponseCode();
            return (responseCode == HttpURLConnection.HTTP_OK);
        } catch (IOException e) {
            return false;
        }
    }
}
