package lektionthree;

import lektionthree.exceptions.GroupOverflowException;
import lektionthree.exceptions.StudentNotFoundException;

public class Main {
    public static void main(String[] args) {
        // Создаем объекты студентов
        Student student1 = new Student("John", "Doe", Gender.MALE, 1, "Group 1");
        Student student2 = new Student("Jane", "Smith", Gender.FEMALE, 2, "Group 1");
        Student student3 = new Student("Alex", "Johnson", Gender.MALE, 3, "Group 1");
        Student student4 = new Student("Emily", "Brown", Gender.FEMALE, 4, "Group 1");
        Student student5 = new Student("Michael", "Davis", Gender.MALE, 5, "Group 1");
        Student student6 = new Student("Emma", "Wilson", Gender.FEMALE, 6, "Group 1");
        Student student7 = new Student("Daniel", "Taylor", Gender.MALE, 7, "Group 1");
        Student student8 = new Student("Olivia", "Anderson", Gender.FEMALE, 8, "Group ");
        Student student9 = new Student("William", "Thomas", Gender.MALE, 9, "Group 1");
        Student student10 = new Student("Sophia", "Martinez", Gender.FEMALE, 10, "Group 1");
        Student student11 = new Student("Sophia", "Martinez", Gender.FEMALE, 10, "Group 1");

        // Создаем объект группы
        Group group = null;
        try {
            group = new Group("Group 1", null);
        } catch (GroupOverflowException e) {
            System.out.println("Error: " + e.getMessage());
        }

        try {
            // Добавляем студентов в группу
            group.addStudent(student1);
            group.addStudent(student2);
            group.addStudent(student3);
            group.addStudent(student4);
            group.addStudent(student5);
            group.addStudent(student6);
            group.addStudent(student7);
            group.addStudent(student8);
            group.addStudent(student9);
            group.addStudent(student10);


            // Попытка добавить студента в полную группу
            group.addStudent(student11);
        } catch (GroupOverflowException e) {
            System.out.println("Group Overflow: " + e.getMessage());
        }

        System.out.println(group);

        try {
            // Поиск студента по фамилии
            Student foundStudent = group.searchStudentByLastName("Smith");
            System.out.println("Found Student: " + foundStudent);

            // Поиск несуществующего студента по фамилии
            foundStudent = group.searchStudentByLastName("Johnson12");
            System.out.println("Found Student: " + foundStudent);
        } catch (StudentNotFoundException e) {
            System.out.println("Student Not Found: " + e.getMessage());
        }

        // Удаление существующего студента по ID
        boolean removed = group.removeStudentById(2);
        System.out.println("Student Removed: " + removed);

        // Удаление несуществующего студента по ID
        removed = group.removeStudentById(13);
        System.out.println("Student Removed: " + removed);

        // Вывод обновленной информации о группе
        System.out.println(group);
    }
}
