package lektionthree;

public class Student extends Human {
    private int id;
    private String groupName;

    public Student(String name, String lastName, Gender gender, int id, String groupName) {
        super(name, lastName, gender);
        setId(id);
        this.groupName = groupName;
    }

    public Student() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if (id < 0) {
            throw new IllegalArgumentException("Id can not be under, or equal 0");
        }
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return String.format("%s{name='%s', lastName='%s', gender='%s', id='%d', groupName='%s'}\n",
                getClass().getSimpleName(),
                getName(),
                getLastName(),
                getGender(),
                getId(),
                getGroupName());
    }
}
