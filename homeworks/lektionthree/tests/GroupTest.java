package lektionthree.tests;

import lektionthree.Gender;
import lektionthree.Group;
import lektionthree.Student;
import lektionthree.exceptions.GroupOverflowException;
import lektionthree.exceptions.StudentNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GroupTest {
    private Group group;

    @Before
    public void setUp() throws GroupOverflowException {
        group = new Group("Group 1", null);
    }

    @Test
    public void testAddStudent() throws GroupOverflowException {
        Student student = new Student("John", "Doe", Gender.MALE, 1, "Group 1");
        group.addStudent(student);
        Student[] students = group.getStudents();
        Assert.assertEquals(10, students.length);
        Assert.assertEquals(null, students[8]);
    }

    @Test(expected = GroupOverflowException.class)
    public void testAddStudentGroupOverflow() throws GroupOverflowException {
        for (int i = 0; i < 10; i++) {
            Student student = new Student("Student " + i, "Lastname " + i, Gender.MALE, i, "Group 1");
            group.addStudent(student);
        }
        Student extraStudent = new Student("Extra", "Student", Gender.MALE, 11, "Group 1");
        group.addStudent(extraStudent);
    }

    @Test
    public void testSearchStudentByLastName() throws GroupOverflowException, StudentNotFoundException {
        Student student1 = new Student("John", "Doe", Gender.MALE, 1, "Group 1");
        Student student2 = new Student("Jane", "Smith", Gender.FEMALE, 2, "Group 1");
        group.addStudent(student1);
        group.addStudent(student2);

        Student foundStudent = group.searchStudentByLastName("Smith");
        Assert.assertEquals(student2, foundStudent);
    }

    @Test(expected = StudentNotFoundException.class)
    public void testSearchStudentByLastNameNotFound() throws GroupOverflowException, StudentNotFoundException {
        Student student1 = new Student("John", "Doe", Gender.MALE, 1, "Group 1");
        Student student2 = new Student("Jane", "Smith", Gender.FEMALE, 2, "Group 1");
        group.addStudent(student1);
        group.addStudent(student2);

        group.searchStudentByLastName("Johnson12");
    }

    @Test
    public void testRemoveStudentById() throws GroupOverflowException {
        Student student1 = new Student("John", "Doe", Gender.MALE, 1, "Group 1");
        Student student2 = new Student("Jane", "Smith", Gender.FEMALE, 2, "Group 1");
        group.addStudent(student1);
        group.addStudent(student2);

        boolean removed = group.removeStudentById(2);
        Assert.assertTrue(removed);
        Student[] students = group.getStudents();
        Assert.assertEquals(10, students.length);
        Assert.assertEquals(student1, students[0]);
    }

    @Test
    public void testRemoveStudentByUnexistableID() throws GroupOverflowException {
        Student student1 = new Student("John", "Doe", Gender.MALE, 1, "Group 1");
        Student student2 = new Student("Jane", "Smith", Gender.FEMALE, 2, "Group 1");
        group.addStudent(student1);
        group.addStudent(student2);

        boolean removed = group.removeStudentById(5);
        Assert.assertFalse(removed);
        Student[] students = group.getStudents();
        Assert.assertEquals(10, students.length);
        Assert.assertEquals(student1, students[0]);
        Assert.assertEquals(student2, students[1]);
    }

    @Test
    public void testToString() throws GroupOverflowException {
        Student student1 = new Student("John", "Doe", Gender.MALE, 1, "Group 1");
        Student student2 = new Student("Jane", "Smith", Gender.FEMALE, 2, "Group 1");
        group.addStudent(student1);
        group.addStudent(student2);

        String expectedOutput = "Group: Group 1\n" +
                "Students:\n" +
                "Name: John Doe, Gender: MALE, ID: 1\n" +
                "Name: Jane Smith, Gender: FEMALE, ID: 2\n";

        Assert.assertEquals(expectedOutput, group.toString());
    }
}
