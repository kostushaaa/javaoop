package lektioneight.students;

import java.util.Objects;

public class Human {
    private String name;
    private String lastName;
    private Gender gender;

    public Human(String name, String lastName, Gender gender) {
        this.name = name;
        this.lastName = lastName;
        this.gender = gender;
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return String.format("%s{name='%s', lastName='%s', gender='%s'}",
                getClass().getSimpleName(),
                getName(),
                getLastName(),
                getGender());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Human otherHuman = (Human) obj;
        if (!Objects.equals(name, otherHuman.name)) return false;
        if (!Objects.equals(lastName, otherHuman.lastName)) return false;
        return gender == otherHuman.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, gender);
    }
}
